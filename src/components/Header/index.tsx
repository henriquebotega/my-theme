import { shade } from "polished";
import React, { useContext } from "react";
import Switch from "react-switch";
import { ThemeContext } from "styled-components";
import { Container } from "./styles";

interface Props {
	toggleTheme(): void;
}

const Header: React.FC<Props> = ({ toggleTheme }) => {
	const context = useContext(ThemeContext);

	return (
		<Container>
			Hello World
			<Switch onChange={toggleTheme} checked={context.title === "light"} checkedIcon={false} uncheckedIcon={false} height={10} width={40} handleDiameter={20} offColor={shade(0.15, context.colors.primary)} onColor={context.colors.secondary} />
		</Container>
	);
};

export default Header;
