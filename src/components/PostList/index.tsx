import React, { useEffect, useState } from "react";
import PostItem from "../PostItem";

export default function PostList() {
	const [loading, setLoading] = useState(false);
	const [posts, setPosts] = useState([]);
	const [newPost, setNewPost] = useState("");

	useEffect(() => {
		setLoading(true);

		fetch("https://jsonplaceholder.typicode.com/posts").then((res: any) => {
			res.json().then((data: any) => {
				setPosts(data);
				setLoading(false);
			});
		});
	}, []);

	return (
		<>
			<input value={newPost} onChange={(e) => setNewPost(e.target.value)} />
			<ul>{!loading && posts.map((obj: any) => <PostItem post={obj} />)}</ul>
		</>
	);
}
