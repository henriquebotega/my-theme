import React, { memo } from "react";

const PostItem = (props: any) => {
	return <li key={props.post.id}>{props.post.title}</li>;
};

export default memo(PostItem);
